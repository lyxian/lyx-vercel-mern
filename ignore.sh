#!/bin/bash

BRANCH_MODE=`echo $VERCEL_GIT_COMMIT_REF | cut -d '_' -f2`
BRANCH_APP=`echo $VERCEL_GIT_COMMIT_REF | cut -d '/' -f2`
echo "BRANCH_MODE: $BRANCH_MODE=prod"
echo "BRANCH_APP: $BRANCH_APP=$APP"

if [[ "$BRANCH_MODE" == "prod" ]] && [[ "$BRANCH_APP" == "$APP" ]]; then
  # Proceed with the build
  echo "✅ - Build can proceed"
  exit 1;

else
  # Don't build
  echo "🛑 - Build cancelled"
  exit 0;
fi